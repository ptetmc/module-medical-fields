(function($) {
  $(function(){
    $('table.medical-genetics').each(function(){
      var $table = $(this);
      $table.find('td.medical-genetics-radio input').change(function(e){
        var $input = $(this),
          $row = $input.closest('tr'),
          value = $row.find('td.medical-genetics-radio input:checked').val(),
          show = (value == '1' || value == '-1');
//        console.log('change', $input.attr('name'), value, show);
        $row.find('td.medical-genetics-result > div').toggle(show).find('textarea').attr('disabled', !show);
      }).bind('state:disabled', function(e) {
        var $input = $(this);
        if (!e.value && $input.closest('td').is('.medical-genetics-na')) $input.trigger('change');
      });
      $table.find('td.medical-genetics-na input').trigger('change');
      $table.find('tr.medical-genetics-additional-gene').each(function(i, tr){
        var $tr = $(tr), $pr = $tr.prev();
        if (!$pr.hasClass('medical-genetics-additional-gene')) return;
        $pr.find('td:first input.form-text').bind('change keyup', function(){
          if ($(this).val().length) {
            $tr.show();
          } else if (!$tr.find('td:first input.form-text').val().length) {
            $tr.hide();
            $pr.prev().filter('.medical-genetics-additional-gene').find('td:first input.form-text').change();
          }
        }).change();
      });
    });
  });
})(jQuery);
